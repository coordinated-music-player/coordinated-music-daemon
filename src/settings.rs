use std;
use std::fs::File;
use std::io::Write;
use std::io::BufReader;
use std::path::PathBuf;
use getopts::Options;

use webapi::*;

#[derive(Debug, Clone)]
pub struct ServerSettings {
    pub token: String,
    pub server: String,
    pub settings_folder: PathBuf,
    pub cache_folder: PathBuf,
}

impl ServerSettings {
    pub fn websocket(&self) -> String {
        self.server.replace("http://", "ws://").replace("https://", "wss://")
    }

    pub fn parse_arguments() -> Result<ServerSettings, ArgumentParseError> {
        let args: Vec<String> = std::env::args().collect();

        let mut opts = Options::new();
        opts.optflag("h", "help", "Print this help message");
        opts.optopt("s", "server", "REQUIRED Specify the server to connect to (https://www.example.com/)", "SERVER");
        opts.optopt("n", "name", "Set the name to use for this agent.  This is required when first establishing a connection to a server to get a token.  When reconnecting using a token, the name will be provided by the server.", "NAME");
        opts.optopt("t", "settings", "Specify a settings folder to use.  Default is ~/.config/coordinated-music/", "SETTINGS_FOLDER");
        opts.optopt("c", "cache", "Specify a cache folder to use.  Default is ~/.config/coordmusic/cache/", "CACHE_FOLDER");
        let usage = opts.usage("Usage for cmd:\nRegister:  cmd --server https://example.com --name \"My Agent Name\"\nReconnect: cmd --server https://example.com");

        let matches = match opts.parse(&args[1..]) {
            Ok(m) => { m }
            Err(f) => { panic!(f.to_string()) }
        };

        if matches.opt_present("h") {
            return Err(ArgumentParseError::NeedHelp("".to_string(), usage.clone()));
        }

        let name = matches.opt_str("n");
        let server = try!(matches.opt_str("s").ok_or(ArgumentParseError::NeedHelp("You must always specify a server to connect to".to_string(), usage.clone())));
        let settings_folder = matches.opt_str("t").map(|t| {
            PathBuf::from(t)
        }).unwrap_or({
            let mut home = std::env::home_dir().expect("Unable to determine what your home directory is");
            home.push(".config");
            home.push("coordmusic");
            home
        });

        let cache_folder = matches.opt_str("c").map(|t| {
            PathBuf::from(t)
        }).unwrap_or({
            let mut home = std::env::home_dir().expect("Unable to determine what your home directory is");
            home.push(".config");
            home.push("coordmusic");
            home.push("cache");
            home
        });

        let mut token_file = settings_folder.clone();
        token_file.push("token");
        info!("Looking for token in {:?}", token_file);
        let token = try!(Self::get_stored_token(&settings_folder).map_err(|e| {
            error!("Error: {:?}", e);
            match name {
                Some(ref n) => ArgumentParseError::RequestToken(
                    TokenRequestSettings{
                        agent_name: n.clone(),
                        server: server.clone(),
                        settings_folder: settings_folder.clone(),
                    }
                ),
                None => ArgumentParseError::NeedHelp("You must specify a name to get a token".to_string(), usage.clone()),
            }
        }));

        Ok(ServerSettings {
            token: token,
            server: server,
            settings_folder: settings_folder,
            cache_folder: cache_folder,
        })
    }

    pub fn get_stored_token(settings_folder: &PathBuf) -> std::io::Result<String> {
        use std::io::prelude::*;

        let mut path = settings_folder.clone();
        path.push("token");
        let f = try!(File::open(path));
        let mut reader = BufReader::new(f);
        let mut line = String::new();
        try!(reader.read_line(&mut line));
        Ok(line.trim().to_string())
    }
}

pub struct TokenRequestSettings {
    pub agent_name: String,
    pub server: String,
    pub settings_folder: PathBuf,
}

impl TokenRequestSettings {
    pub fn download_token(&self) -> Result<(), RegistrationError> {
        fn save_token(token: String, settings_folder: &PathBuf) -> std::io::Result<()> {
            let mut token_path = settings_folder.clone();
            token_path.push("token");
            let mut f = try!(File::create(&token_path));
            try!(writeln!(f, "{}", token));
            Ok(())
        }

        let token = register(&self.server[..], &self.agent_name[..]);
        match token {
            Ok(t) => {
                trace!("We should write the token to disk");
                let result = save_token(t, &self.settings_folder);
                info!("Writing token to disk: {:?}", result);
                result.map_err(|e| RegistrationError::IoError(e))
            }
            Err(e) => Err(e)
        }
    }
}

pub enum ArgumentParseError {
    NeedHelp(String, String),
    RequestToken(TokenRequestSettings),
}
