extern crate gst;
extern crate websocket;

use std::thread;
use std::sync::mpsc::{channel, Sender};
use std::time::Duration;
use std::path::PathBuf;

use playlist::PlaylistMessage;
use webapi::*;
use socket::SocketMessage;
use settings::ServerSettings;

pub enum PlayerMessage {
    Seek(f64),
    Pause,
    UpdatePosition,
    RequestConnectInfo,
    Quit(QuitData),
    GstMessage(gst::Message),
}

pub struct QuitData {
    send_position: bool,
    send_drop: bool,
}

impl QuitData {
    pub fn no_update() -> QuitData {
        QuitData {
            send_position: false,
            send_drop: false,
        }
    }

    pub fn with_update() -> QuitData {
        QuitData {
            send_position: true,
            send_drop: false,
        }
    }

    pub fn with_drop() -> QuitData {
        QuitData {
            send_position: true,
            send_drop: true,
        }
    }
}

#[derive(Debug)]
pub enum PlaySongError {
    ErrorParsingMediaFile(PathBuf),
    GstError(gst::Error),
}

pub fn play_song(queue: PlayQueue, tx_playlist: Sender<PlaylistMessage>, sender: Sender<SocketMessage>, server: &ServerSettings) -> Result<Sender<PlayerMessage>, PlaySongError> {
    use gst::ElementT;
    use gst::Pipeline;

    let (tx, rx) = channel();

    let mut media_file = server.cache_folder.clone();
    media_file.push(queue.current_song().path);
    let file_path = try!(
        media_file.to_str().ok_or(PlaySongError::ErrorParsingMediaFile(media_file.clone()))
    );

    gst::init();
    let uri = try!(gst::filename_to_uri(file_path).map_err(|e| PlaySongError::GstError(e)));
    debug!("uri: '{}'", uri);

    let pipeline_str: String = format!("filesrc location=\"{}\" ! decodebin ! audioconvert ! audioresample ! pulsesink", file_path);
    debug!("Pipeline: {}", pipeline_str);

    let mut pipeline: Pipeline = try!(gst::Pipeline::new_from_str(pipeline_str.as_str()).map_err(|e| PlaySongError::GstError(e)));
    let mut mainloop: gst::MainLoop = gst::MainLoop::new();
    let mut bus: gst::bus::Bus = pipeline.bus().expect("Couldn't get pipeline bus");
    mainloop.spawn();
    pipeline.play();

    let tx_seek = tx.clone();
    {
        let seconds = queue.position.as_secs() as f64;
        thread::spawn(move || {
            // TODO: Use state change instead of a timer
            thread::sleep(Duration::from_millis(100));
            info!("Seeking to {}", seconds);
            let _ = tx_seek.send(PlayerMessage::Seek(seconds));
        });
    }

    debug!("Ready to start a new thread for the player");
    thread::spawn(move || {
        loop {
            match rx.recv() {
                Ok(PlayerMessage::GstMessage(message)) => {
                    match message {
                        gst::Message::StateChangedParsed{ref old, ref new, ..} => {
                            debug!("State Change Received.  element `{}` changed from {:?} to {:?}", message.src_name(), old, new);
                        }
                        gst::Message::ErrorParsed{ref error, ..} => {
				            error!("error msg from element `{}`: {}, quitting", message.src_name(), error.message());
                            break;
                        }
                        gst::Message::Eos(_) => {
                            debug!("eos received quiting");
                            let _ = tx_playlist.send(PlaylistMessage::Next);
                            let _ = sender.send(SocketMessage::Json(command_agent_state(PlayState::Stop, Duration::from_secs(pipeline.position_s().unwrap_or(0_f64) as u64))));
                            break;
                        }
                        _ => {
                            warn!("unknown gst message received");
                        }
                    }
                }
                Ok(PlayerMessage::Seek(to)) => {
                    pipeline.set_position_s(to);
                    let _ = sender.send(SocketMessage::Json(command_agent_state(PlayState::Play, Duration::from_secs(to as u64))));
                }
                Ok(PlayerMessage::Pause) => {
                    if pipeline.is_playing() {
                        debug!("Pausing playback");
                        let _ = sender.send(SocketMessage::Json(command_position(pipeline.position_s().unwrap_or(0_f64), queue.song, queue.name.clone())));
                        let _ = sender.send(SocketMessage::Json(command_agent_state(PlayState::Pause, Duration::from_secs(pipeline.position_s().unwrap_or(0_f64) as u64))));
                        pipeline.pause();
                    } else if pipeline.is_paused() {
                        debug!("Resuming playback");
                        let _ = sender.send(SocketMessage::Json(command_agent_state(PlayState::Play, Duration::from_secs(pipeline.position_s().unwrap_or(0_f64) as u64))));
                        pipeline.play();
                    }
                }
                Ok(PlayerMessage::Quit(data)) => {
                    trace!("Channel to media player has hung up");
                    if data.send_position {
                        let _ = sender.send(SocketMessage::Json(command_position(pipeline.position_s().unwrap_or(0_f64), queue.song, queue.name.clone())));
                    }
                    if data.send_drop {
                        trace!("We should drop this queue too");
                        let _ = sender.send(SocketMessage::Json(command_drop_queue()));
                    }
                    let _ = sender.send(SocketMessage::Json(command_agent_state(PlayState::Stop, Duration::from_secs(pipeline.position_s().unwrap_or(0_f64) as u64))));
                    let duration: f64 = pipeline.duration_s().unwrap_or(0_f64);
                    pipeline.set_position_s(duration);
                    break;
                }
                Ok(PlayerMessage::UpdatePosition) => {
                    trace!("Updating time");
                    let _ = sender.send(SocketMessage::Json(command_position(pipeline.position_s().unwrap_or(0_f64), queue.song, queue.name.clone())));
                }
                Ok(PlayerMessage::RequestConnectInfo) => {
                    if pipeline.is_playing() {
                        let _ = sender.send(SocketMessage::Json(command_agent_state(PlayState::Play, Duration::from_secs(pipeline.position_s().unwrap_or(0_f64) as u64))));
                    } else {
                        let _ = sender.send(SocketMessage::Json(command_agent_state(PlayState::Stop, Duration::from_secs(pipeline.position_s().unwrap_or(0_f64) as u64))));
                    }
                }
                Err(msg) => {
                    trace!("Player channel error: {}", msg);
                    break;
                }
            }
        }
        trace!("Ending player loop !!!");
        let duration: f64 = pipeline.duration_s().unwrap_or(0_f64);
        pipeline.set_position_s(duration);
        mainloop.quit();
    });

    let (tx_unblock, rx_unblock) = channel();
    {
        let tx1 = tx.clone();
        thread::spawn(move || {
            debug!("Listening for gst messages");
            let bus_receiver: gst::bus::Receiver = bus.receiver();
            for message in bus_receiver.iter().map(|m| m.parse()) {
                debug!("Sending gst message to player");
                if let gst::Message::StateChangedParsed{new, ..} = message {
                    match new {
                        gst::GstState::GST_STATE_PLAYING => {
                            let _ = tx_unblock.send(());
                        }
                        _ => {}
                    }
                }
                let _ = tx1.send(PlayerMessage::GstMessage(message));
            }
            let _ = tx1.send(PlayerMessage::Quit(QuitData::with_update()));
            trace!("Finishing player message loop !!!");
        });
    }

    {
        use time::PreciseTime;
        use time::Duration;
        let start = PreciseTime::now();
        let mut done = false;
        while !done && start.to(PreciseTime::now()) < Duration::seconds(1) {
            match rx_unblock.try_recv() {
                Ok(()) => { done = true; }
                _ => {}
            }
        }
    }

    Ok(tx)
}

pub enum PlayState {
    Play,
    Pause,
    Stop,
}

impl PlayState {
    pub fn to_printable_string(&self) -> String {
        match *self {
            PlayState::Play => "play".to_string(),
            PlayState::Pause => "pause".to_string(),
            PlayState::Stop => "stop".to_string(),
        }
    }
}
