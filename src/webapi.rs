extern crate hyper;
extern crate rustc_serialize;
extern crate url;

use std::collections::BTreeMap;
use std::iter::Iterator;
use std::time::Duration;
use std::path::Path;
use std::path::PathBuf;
use std::fs::create_dir_all;
use std::fs::File;
use std::io::Read;
use std::io::Write;
use std;

use hyper::header::Connection;
use hyper::client::response::Response;

use rustc_serialize::json::Json;
use rustc_serialize::json::ToJson;

use player::PlayState;
use settings::ServerSettings;

pub fn command_position<S: Into<String>>(seconds: f64, song_number: usize, queue_name: S) -> Json {
    trace!("Building json command position");
    let mut json = BTreeMap::new();
    json.insert("command".to_string(), "position".to_string().to_json());
    json.insert("queue".to_string(), queue_name.into().to_json());
    json.insert("song".to_string(), song_number.to_json());
    json.insert("seconds".to_string(), seconds.to_json());
    Json::Object(json)
}

pub fn command_drop_queue() -> Json {
    trace!("Building json command drop queue");
    let mut t = BTreeMap::new();
    t.insert("command".to_string(), "queue_dropped".to_string().to_json());
    Json::Object(t)
}

pub fn command_agent_state(state: PlayState, position: Duration) -> Json {
    trace!("Building json command agent state");
    let mut t = BTreeMap::new();
    t.insert("command".to_string(), "update_state".to_string().to_json());
    t.insert("playState".to_string(), state.to_printable_string().to_json());
    t.insert("seconds".to_string(), position.as_secs().to_json());
    Json::Object(t)
}

pub fn command_info<S: Into<String>>(msg: S) -> Json {
    trace!("Building json command play state");
    let mut t = BTreeMap::new();
    t.insert("command".to_string(), "info".to_string().to_json());
    t.insert("message".to_string(), msg.into().to_json());
    Json::Object(t)
}

#[derive(Debug)]
pub enum RegistrationError {
    ParserError(rustc_serialize::json::ParserError),
    IoError(std::io::Error),
    HyperError(hyper::Error),
    TokenNotFoundError,
    TokenInvalidError,
}

#[derive(Debug)]
pub enum EndpointError {
    IoError(std::io::Error),
    HyperError(hyper::Error),
    DirReadError(String),
    WriteError(std::io::Error),
}

pub fn register<S: Into<String>>(server: S, name: S) -> Result<String, RegistrationError> {
    use hyper::Client;

    let client = Client::new();

    let name = name.into();
    let url = &format!("{}/auth/register/{}", server.into(), name)[..];
    info!("Registering as {}", name);
    let mut res = try!(client.get(url)
        .header(Connection::close())
        .send().map_err(|e| RegistrationError::HyperError(e)));
    let mut response = String::new();
    let _ = res.read_to_string(&mut response);
    info!("Response for registration is {}", response);
    let response_str: &str = &response[..];

    let json: Result<Json, RegistrationError> = Json::from_str(response_str).map_err(|e| RegistrationError::ParserError(e));
    let found: Result<Json, RegistrationError> = json.and_then(|js| { js.find("token").map(|t| { t.clone() }).ok_or(RegistrationError::TokenNotFoundError) });
    let token: Result<String, RegistrationError> = found
        .and_then(|js_val| { js_val.as_string().map(|v| { v.to_string() }).ok_or(RegistrationError::TokenInvalidError) })
        .map(|s| { s.to_string() });
    token
}

pub fn ensure_song_in_cache(file: String, server: &ServerSettings) -> bool {
    let mut file_path_buf: PathBuf = server.cache_folder.clone();
    file_path_buf.push(Path::new(file.as_str()));
    let file_path: &Path = file_path_buf.as_path();
    if !file_path.exists() {
        let result = download(file.as_str(), server);
        result.unwrap_or_else(|e| { error!("Error downloading a media file: {:?}", e); });
    }
    info!("Does it exist now? {}", file_path.exists());
    return file_path.exists();
}

use url::percent_encoding::SIMPLE_ENCODE_SET;
define_encode_set! {
    pub MEDIA_ENCODE_SET = [SIMPLE_ENCODE_SET] | {'[', ']', '#', '"', '?', '{', '}', ' '}
}

#[derive(Clone, Debug)]
struct TokenHeader {
    token: String,
}

impl TokenHeader {
    fn new(server: &ServerSettings) -> TokenHeader {
        TokenHeader {
            token: server.token.clone(),
        }
    }
}

impl hyper::header::HeaderFormat for TokenHeader {
    fn fmt_header(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(fmt, "{}", self.token)
    }
}

impl hyper::header::Header for TokenHeader {
    fn header_name() -> &'static str {
        "token"
    }

    fn parse_header(raw: &[Vec<u8>]) -> hyper::error::Result<Self> {
        use hyper::header::parsing::from_one_raw_str;
        
        if raw.len() == 1 {
            // do the right thing
            let token = from_one_raw_str(raw);
            token.map(|t| TokenHeader { token: t })
        } else {
            return Err(hyper::error::Error::Header)
        }
    }
}

pub fn download(file: &str, server: &ServerSettings) -> Result<(), EndpointError> {
    use hyper::Client;
    use url::percent_encoding::utf8_percent_encode;

    let client: Client = Client::new();

    let url: &str = &format!("{}", utf8_percent_encode(&format!("{}/api/fetch/{}", server.server, file)[..], MEDIA_ENCODE_SET))[..];
    info!("Fetching {}", url);
    let mut res: Response = try!(client.get(url)
        .header(TokenHeader::new(server))
        .header(Connection::close())
        .send().map_err(|e| EndpointError::HyperError(e)));

    // Read the Response.
    let mut file_bytes: Vec<u8> = Vec::new();
    let size: usize = try!(res.read_to_end(&mut file_bytes).map_err(|e| EndpointError::IoError(e)));
    debug!("The size of the file is {}", size);

    let mut file_path_buf: PathBuf = server.cache_folder.clone();
    file_path_buf.push(Path::new(file));
    let file_path: &Path = file_path_buf.as_path();
    let dir: &Path = try!(file_path.parent().ok_or(EndpointError::DirReadError(file.to_string())));
    try!(create_dir_all(dir).map_err(|e| EndpointError::WriteError(e)));
    let mut f = try!(File::create(file_path).map_err(|e| EndpointError::IoError(e)));
    try!(f.write(file_bytes.as_slice()).map_err(|e| EndpointError::WriteError(e)));
    Ok(())
}

#[derive(Debug, Clone)]
pub struct PlayQueue {
    pub name: String,
    pub song: usize,
    pub queue: Vec<PlayQueueItem>,
    pub position: Duration,
}

#[derive(Debug, Clone)]
pub struct PlayQueueItem {
    pub path: String,
    pub duration: Duration,
}

impl PlayQueue {
    pub fn from_json(json: &Json) -> Option<PlayQueue> {
        match json.find("queue").and_then(|j| j.find("id")).and_then(|j| j.as_string()) {
            Some(name) => {
                let milliseconds: u64 = (json.find("queue").and_then(|j| j.find("seconds")).and_then(|j| j.as_f64()).unwrap_or(0_f64) * 1000_f64).round() as u64;
                let queue: Option<Vec<PlayQueueItem>> = json.find("queue").and_then(|j| j.find("queue")).and_then(|j| j.as_array()).map(|j| j.iter().filter_map(|i| {
                    i.find("path").and_then(|p| p.as_string()).map(|p| {
                        PlayQueueItem {
                            path: p.to_string(),
                            duration: Duration::from_secs(i.find("secondsDuration").and_then(|s| s.as_u64()).unwrap_or(0)),
                        }
                    })
                }).collect());
                Some(PlayQueue {
                    name: name.to_string(),
                    song: json.find("queue").and_then(|j| j.find("song")).and_then(|j| j.as_u64()).unwrap_or(0_u64) as usize,
                    position: Duration::from_millis(milliseconds),
                    queue: queue.unwrap_or(Vec::new()),
                })
            }
            None => {
                None
            }
        }
    }

    pub fn current_song(&self) -> PlayQueueItem {
        self.queue[self.song].clone()
    }

    pub fn next_songs(&self, count: usize) -> Vec<PlayQueueItem> {
        self.queue.iter().skip(self.song).take(count).map(|i| i.clone()).collect()
    }

    pub fn increment(&mut self) -> bool {
        if (self.song + 1) < self.queue.len() {
            self.song += 1;
            self.position = Duration::from_secs(0);
            true
        } else {
            false
        }
    }

    pub fn decrement(&mut self) -> bool {
        if self.song > 0 {
            self.song -= 1;
            self.position = Duration::from_secs(0);
            true
        } else {
            false
        }
    }

    pub fn jump(&mut self, song: usize) -> bool {
        if song < self.queue.len() {
            self.song = song;
            true
        } else {
            false
        }
    }
}
