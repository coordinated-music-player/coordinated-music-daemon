extern crate websocket;
extern crate gst;
extern crate hyper;
extern crate rustc_serialize;

use std::thread;
use std::sync::mpsc::{channel, Sender};
use std::io::stdin;
use std::str::from_utf8;
use std::time::Duration;

use rustc_serialize::json::Json;

use websocket::{Message as WsMessage, Sender as WsSender, Receiver as WsReceiver};
use websocket::message::Type;
use websocket::client::request::Url;
use url::percent_encoding::utf8_percent_encode;
use url::percent_encoding::PATH_SEGMENT_ENCODE_SET;

use webapi::*;
use playlist::*;
use settings::*;

pub enum SocketMessage {
    Json(Json),
    Reconnect,
    Close,
}

pub fn connect(settings: &ServerSettings) {
    let tx = socket_keep_alive_loop(settings);

    loop {
        let mut input = String::new();
        match stdin().read_line(&mut input) {
            Ok(_) => {
                let trimmed = input.trim();
                match trimmed {
                    "/close" | "/quit" => {
                        // Close the connection
                        let _ = tx.send(SocketMessage::Close);
                        break;
                    }
                    // Otherwise, do nothing
                    _ => {}
                };
            }
            _ => {}
        }
    }

    info!("Waiting for child threads to exit");
}

fn socket_keep_alive_loop(settings: &ServerSettings) -> Sender<SocketMessage> {
    let (tx, rx) = channel();
    let tx_pl = playlist_controller(tx.clone(), settings);
    let mut sender: websocket::client::Sender<websocket::WebSocketStream> = try_reconnect(settings, tx.clone(), tx_pl.clone(), command_info("Initial Connect"));
    let tx_self = tx.clone();
    let settings_copy = settings.clone();
    thread::spawn(move || {
        loop {
            // Send loop
            match rx.recv() {
                Ok(SocketMessage::Json(m)) => {
                    info!("Sending message to server: {}", m.to_string());
                    match sender.send_message(&WsMessage::text(m.to_string())) {
                        Ok(()) => {}
                        Err(e) => {
                            error!("Send Loop Error: {:?}", e);
                            error!("Closing Connection");
                            sender = try_reconnect(&settings_copy, tx_self.clone(), tx_pl.clone(), m);
                            let _ = tx_pl.send(PlaylistMessage::RequestConnectInfo);
                        }
                    }
                }
                Ok(SocketMessage::Reconnect) => {
                    sender = try_reconnect(&settings_copy, tx_self.clone(), tx_pl.clone(), command_info("Reconnected".to_string()));
                    let _ = tx_pl.send(PlaylistMessage::RequestConnectInfo);
                }
                Ok(SocketMessage::Close) => {
                    let _ = sender.send_message(&WsMessage::close());
                    break;
                }
                Err(e) => {
                    error!("Send Loop: {:?}", e);
                    let _ = sender.send_message(&WsMessage::close());
                }
            };
            info!("socket keep alive loop end !!!");
        }
    });
    tx
}

fn try_reconnect(settings: &ServerSettings, tx_keep_alive: Sender<SocketMessage>, tx_pl: Sender<PlaylistMessage>, connect_message: Json) -> websocket::sender::Sender<websocket::WebSocketStream> {
    // try to reconnect.
    loop {
        info!("Attempting a reconnect");
        match socket_loop(settings, tx_keep_alive.clone(), tx_pl.clone()) {
            Ok(mut sender) => {
                match sender.send_message(&WsMessage::text(connect_message.to_string())) {
                    Ok(_) => { return sender; }
                    Err(e) => {
                        error!("Error sending after reconnecting.  Let's try reconnecting again. ({:?})", e);
                    }
                }
            }
            Err(e) => {
                match e {
                    websocket::result::WebSocketError::ResponseError(s) => { error!("We got back a {} ({}).  This most likely means that the token must be approved.", e, s); }
                    _ => { error!("Error trying to connect to socket: {}", e); }
                }
                thread::sleep(Duration::from_secs(5));
            }
        }
    }
}

fn socket_loop(settings: &ServerSettings, tx_keep_alive: Sender<SocketMessage>, tx_pl: Sender<PlaylistMessage>) -> Result<websocket::sender::Sender<websocket::WebSocketStream>, websocket::result::WebSocketError> {
    use websocket::Client;

    let token_encoded: &str = &format!("{}", utf8_percent_encode(&settings.token[..], PATH_SEGMENT_ENCODE_SET))[..];
    let url = try!(Url::parse(&format!("{}/agent/connect/{}", settings.websocket(), token_encoded)[..]));
    info!("Connecting to {}", url);
    let request = try!(Client::connect(url));
    let response = try!(request.send());
    trace!("Validating response...");
    try!(response.validate()); // Validate the response
    trace!("Successfully connected");
    let (sender, mut receiver) = response.begin().split();

    thread::spawn(move || {
        // Receive loop
        for ws_message in receiver.incoming_messages() {
            let message: WsMessage = match ws_message {
                Ok(m) => m,
                Err(e) => {
                    error!("Receive Loop Error: {:?}", e);
                    break;
                }
            };
            match message.opcode {
                Type::Close => {
                    break;
                }
                Type::Text => {
                    let payload_vec: Vec<u8> = message.payload.into_owned();
                    let payload: &str = from_utf8(&payload_vec).unwrap_or("");
                    info!("Received text {}", payload);
                    let json: Option<Json> = Json::from_str(payload).ok();
                    let command: Option<String> = {
                        let cmd = json.clone().and_then(|j: Json| {
                            let result: Option<&Json> = j.find("command");
                            let ret: Option<Json> = result.map(|j| j.clone());
                            ret
                        });
                        let cmd_str: Option<String> = cmd.and_then(|j: Json| j.as_string().map(|s: &str| s.to_string()));
                        cmd_str
                    };
                    debug!("Command is {:?}", command);
                    match command.as_ref().map(String::as_ref) {
                        Some("play") => {
                            let _ = tx_pl.send(PlaylistMessage::Play);
                        }
                        Some("seek") => {
                            let to: Option<f64> = json.and_then(|j| j.find("seconds").and_then(|j| j.as_f64()));
                            match to {
                                Some(to_real) => {
                                    let _ = tx_pl.send(PlaylistMessage::Seek(to_real));
                                }
                                None => {
                                    error!("Unable to seek, no `to` found in '{}'", payload);
                                }
                            }
                        }
                        Some("pause") => {
                            let _ = tx_pl.send(PlaylistMessage::Pause);
                        }
                        Some("stop") => {
                            let _ = tx_pl.send(PlaylistMessage::Stop);
                        }
                        Some("next") => {
                            let _ = tx_pl.send(PlaylistMessage::Next);
                        }
                        Some("prev") => {
                            let _ = tx_pl.send(PlaylistMessage::Prev);
                        }
                        Some("jump") => {
                            let song: Option<u64> = json.and_then(|j| j.find("songPosition").and_then(|j| j.as_u64()));
                            info!("Jumping to song {:?}", song);
                            for s in song {
                                let _ = tx_pl.send(PlaylistMessage::Jump(s as usize));
                            }
                        }
                        Some("queue") => {
                            let queue = json.clone().and_then(|j| PlayQueue::from_json(&j));
                            match queue {
                                Some(q) => { let _ = tx_pl.send(PlaylistMessage::Queue(q)); },
                                None => { error!("Unable to set playlist because it could not be parsed: {:?}", json); },
                            }
                        }
                        Some("dropQueue") => {
                            trace!("Tell someone to drop this queue");
                            let _ = tx_pl.send(PlaylistMessage::DropQueue);
                        }
                        _ => {
                            error!("Unrecognized Command: {:?}", command);
                        }
                    }
                }
                // Say what we received
                _ => info!("Receive Loop: {:?}", message),
            }
        }
        trace!("Finishing websocket receiver loop !!!");
        thread::sleep(Duration::from_secs(3));
        let _ = receiver.shutdown_all();
        {
            let result = tx_keep_alive.send(SocketMessage::Reconnect);
            if result.is_err() {
                error!("Error when trying to tell the socket thread that we should reconnect: {:?}", result);
            }
        }
    });

    Ok(sender)
}
