#[macro_use] extern crate url;

extern crate websocket;
extern crate gst;
extern crate hyper;
extern crate rustc_serialize;
extern crate getopts;
#[macro_use] extern crate log;
extern crate env_logger;
extern crate time;

mod webapi;
mod player;
mod playlist;
mod socket;
mod settings;
mod downloader;

use socket::connect;
use settings::*;

fn main() {
    println!("Starting up");
    env_logger::init().unwrap();
    println!("inited logger");
    debug!("Here is a message");

    match ServerSettings::parse_arguments() {
        Ok(settings) => {
            trace!("Running Coordinated Music Daemon");
            connect(&settings);
            trace!("Terminating Coordinated Music Daemon");
        }
        Err(ArgumentParseError::RequestToken(settings)) => {
            info!("Unable to open an existing token.  We will try to request a new token");
            match settings.download_token() {
                Ok(_) => {
                    println!("A new token has been downloaded");
                }
                Err(e) => {
                    println!("There was an error downloading the new token: {:?}", e);
                }
            }
        }
        Err(ArgumentParseError::NeedHelp(msg, help)) => {
            println!("{}\n\n{}", msg, help);
        }
    }
}
