use std::thread;
use std::sync::mpsc::{channel, Sender};

use playlist::PlaylistMessage;
use webapi::ensure_song_in_cache;
use settings::ServerSettings;

pub enum DownloadMessage {
    PriorityDownload(String, Option<Sender<PlaylistMessage>>),
    RequestDownload(String, Option<Sender<PlaylistMessage>>),
}

enum ActualDownloader {
    Download(String, Option<Sender<PlaylistMessage>>),
}

pub fn downloader_thread(server: &ServerSettings) -> Sender<DownloadMessage> {
    let (tx, rx) = channel();
    let (dl_tx, dl_rx) = channel();
    let server = server.clone();
    let downloader_server = server.clone();

    thread::spawn(move || {
        loop {
            match rx.recv() {
                Ok(DownloadMessage::PriorityDownload(path, callback)) => {
                    ensure_song_in_cache(path.clone(), &server);
                    for cb in callback {
                        let _ = cb.send(PlaylistMessage::MediaReady(path.clone()));
                    }
                }
                Ok(DownloadMessage::RequestDownload(path, callback)) => {
                    let _ = dl_tx.send(ActualDownloader::Download(path, callback));
                }
                Err(_) => {}
            }
        }
    });

    thread::spawn(move || {
        loop {
            match dl_rx.recv() {
                Ok(ActualDownloader::Download(path, callback)) => {
                    ensure_song_in_cache(path.clone(), &downloader_server);
                    for cb in callback {
                        let _ = cb.send(PlaylistMessage::MediaReady(path.clone()));
                    }
                }
                Err(_) => {}
            }
        }
    });

    tx
}
