use std::thread;
use std::sync::mpsc::{channel, Sender};

use webapi::*;
use player::{PlayerMessage, QuitData, play_song};
use socket::SocketMessage;
use settings::ServerSettings;
use downloader::*;

pub enum PlaylistMessage {
    Play,
    Stop,
    Pause,
    Next,
    Prev,
    Jump(usize),
    Seek(f64),
    Queue(PlayQueue),
    DropQueue,
    RequestConnectInfo,
    MediaReady(String),
}

pub fn playlist_controller(tx_to_server: Sender<SocketMessage>, server: &ServerSettings) -> Sender<PlaylistMessage> {
    let (tx, rx) = channel();
    let tx_return = tx.clone();
    let server_cp = server.clone();
    let downloader = downloader_thread(server);

    thread::spawn(move || {
        let (mut tx_p, _) = channel();
        let mut queue: Option<PlayQueue> = None;

        loop {
            match rx.recv() {
                Ok(PlaylistMessage::Play) => {
                    for q in &queue {
                        let _ = downloader.send(DownloadMessage::PriorityDownload(q.current_song().path, Some(tx.clone())));
                    }
                }
                Ok(PlaylistMessage::MediaReady(path)) => {
                    for q in &queue {
                        if path == q.current_song().path {
                            match tx_p.send(PlayerMessage::Quit(QuitData::no_update())) {
                                Ok(_) => { debug!("Successful disconnect on media player"); }
                                Err(e) => {
                                    debug!("Tried to disconnect: {:?}", e);
                                }
                            }
                            match play_song(q.clone(), tx.clone(), tx_to_server.clone(), &server_cp) {
                                Ok(it) => {
                                    let _ = it.send(PlayerMessage::UpdatePosition);
                                    tx_p = it
                                }
                                Err(e) => {
                                    error!("Unable to play song. Error: {:?}", e)
                                }
                            }
                            for next in q.next_songs(5) {
                                let _ = downloader.send(DownloadMessage::RequestDownload(next.path, None));
                            }
                        }
                    }
                }
                Ok(PlaylistMessage::Pause) => {
                    let _ = tx_p.send(PlayerMessage::Pause);
                }
                Ok(PlaylistMessage::Seek(to)) => {
                    let _ = tx_p.send(PlayerMessage::Seek(to));
                }
                Ok(PlaylistMessage::Stop) => {
                    let _ = tx_p.send(PlayerMessage::Quit(QuitData::with_update()));
                }
                Ok(PlaylistMessage::Next) => {
                    for q in &mut queue {
                        if q.increment() {
                            let _ = tx.send(PlaylistMessage::Play);
                        }
                    }
                }
                Ok(PlaylistMessage::Prev) => {
                    for q in &mut queue {
                        if q.decrement() {
                            let _ = tx.send(PlaylistMessage::Play);
                        }
                    }
                }
                Ok(PlaylistMessage::Jump(song)) => {
                    for q in &mut queue {
                        if q.jump(song) {
                            let _ = tx.send(PlaylistMessage::Play);
                        }
                    }
                }
                Ok(PlaylistMessage::Queue(q)) => {
                    info!("playlist is {:?}", q.name);
                    info!("queue is {:?}", q);
                    queue = Some(q);
                }
                Ok(PlaylistMessage::DropQueue) => {
                    trace!("Send a quit with drop");
                    queue = None;
                    if tx_p.send(PlayerMessage::Quit(QuitData::with_drop())).is_err() {
                        // we need to send notification ourselves that we have dropped the queue
                        let _ = tx_to_server.send(SocketMessage::Json(command_drop_queue()));
                    }
                }
                Ok(PlaylistMessage::RequestConnectInfo) => {
                    trace!("Connection info requested");
                    let _ = tx_p.send(PlayerMessage::RequestConnectInfo);
                }
                Err(msg) => {
                    error!("Playlist channel error: {}", msg);
                    break;
                }
            }
        }
        trace!("Ending playlist loop !!!");
    });

    tx_return
}
